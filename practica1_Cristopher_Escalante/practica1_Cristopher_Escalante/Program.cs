﻿using System;
using System.Linq;

namespace practica1_Cristopher_Escalante
{
    class Program
    {
        static void Main(string[] args)
        {
            // ejercicio1();
            ejercicio3();
        }

        public static void ejercicio1()
        {
            // Leer 10 calificaciones e imprimir aprobados y sobresalientes.
            int nota;
            int contador;
            contador = 1;
            int sobresalientes;
            sobresalientes = 0;
            int numeroaprobados;
            numeroaprobados = 0;

            Console.WriteLine("Escribe diez notas");
            while (contador <= 10)
            {
                nota = Int32.Parse(Console.ReadLine());
                if (nota >= 5)
                {
                    numeroaprobados++;
                }
                if (nota >= 9)
                {
                    sobresalientes++;
                }
                contador++;
            }
            Console.WriteLine("El numero de aprobados es " + numeroaprobados);
            Console.WriteLine("El numero de sobresalientes es " + sobresalientes);
        }

        public static void ejercicio2()
        {
            /* DateTime fecha = DateTime.Parse(Console.ReadLine());
             Console.WriteLine("La fecha introducida es " + fecha);
             fecha = fecha.AddDays(90);
             Console.WriteLine("La fecha de entrega es " + fecha);*/

            int dia, mes, anio, mesnuevo, aux;
            dia = Int32.Parse(Console.ReadLine());
            mes = Int32.Parse(Console.ReadLine());
            anio = Int32.Parse(Console.ReadLine());

            if (dia < 1 || dia > 30 || mes > 12 || mes < 1)
            {
                Console.WriteLine("No has insertado una fecha correctamente");
            }
            else
            {
                mesnuevo = mes + 3;
                if (mesnuevo > 12)
                {
                    aux = mesnuevo - 12;
                    mesnuevo = aux;
                    anio++;
                }
                Console.WriteLine("La fecha de pago es: " + dia + "/" + mesnuevo + "/" + anio);
            }


        }

        public static void ejercicio3()
        {
            int[] numeros = new int[10];
            numeros[0] = 8;
            numeros[1] = 12;
            numeros[2] = 4;
            numeros[3] = 0;
            numeros[4] = 3;
            numeros[5] = 9;
            numeros[6] = 14;
            numeros[7] = 7;
            numeros[8] = 5;
            numeros[9] = 13;
            numeros[10] = 11;

            Array.Sort(numeros);
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine(numeros[i]);
            }
        }

        public static void ejercicio4()
        {
            string[] Empresa = new string[10];
            int[] ingresos = new int[10];
            int[] gastos = new int[10];
            int[] Diferencia = new int[10];
            double menor = 0;
            int loc = 0;

            for (int i = 0; i < Empresa.Length; i++)
            {
                Empresa[i] = "La empresa " + (i + 1);
                Console.WriteLine(Empresa[i]);
                Console.WriteLine("Escribe las ganancias mensuales");
                ingresos[i] = int.Parse(Console.ReadLine());
                Console.WriteLine("Coloque los gastos mensuales");
                gastos[i] = int.Parse(Console.ReadLine());
                Diferencia[i] = ingresos[i] - gastos[i];
                if (Diferencia[i] < menor)
                {
                    menor = Diferencia[i];
                    loc = i + 1;
                }
                Console.WriteLine("");
            }
            Console.WriteLine("La empresa " + loc);
            Console.WriteLine("Ha tenido un resultado de ingresos-gastos, con una pérdida de");
            Console.WriteLine(menor);
        }

        public static void ejercicio5()
        {
            int[] valor = new int[10];
            int numerom1 = 0;
            int numerom2 = 0;
            for (int i = 0; i < valor.Length; i++)
            {
                valor[i] = Int32.Parse(Console.ReadLine());
                if (valor[i] > numerom1)
                {
                    numerom1 = valor[i];
                }
                if ((valor[i] > numerom2) && (valor[i] < numerom1))
                {
                    numerom2 = valor[i];
                }
            }
            Console.WriteLine("");
            Console.WriteLine("El primer numero mayor es");
            Console.WriteLine(numerom1);
            Console.WriteLine("");
            Console.WriteLine("El segundo numero mayor es");
            Console.WriteLine(numerom2);
        }
        public static void Ejercicio7()
        {
            int[] notas = new int[10];
            int mayor = 0;
            int menor = 0;
            double media = 0;
            for (int i = 0; i < notas.Length; i++)
            {
                menor = notas[0];
                Console.WriteLine("Escribe la nota " + (i + 1));
                notas[i] = int.Parse(Console.ReadLine());
                if (notas[i] > mayor)          //podia usar notas.Max()
                {
                    mayor = notas[i];
                }
                if ((notas[i] < mayor) && (notas[i] < menor))          //aqui notas.Min()
                {
                    menor = notas[i];
                }
            }
            if (mayor - menor >= 3)
            {
                Console.WriteLine("Se desestimó la nota mayor y la menor debido a su diferencia de 3 puntos");
                media = notas.Sum() - mayor;
                media = media - menor;
                media = media / 8;
                Console.WriteLine("La media es de " + media);
            }
            else
            {
                media = notas.Sum() / 10;
                Console.WriteLine("La media es de " + media);
            }
        }

    }
}
